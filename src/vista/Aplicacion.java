/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import controlador.ControlDom;
import controlador.ControlEscuelasOficialesIdiomas;
import dao.CursoDAO;
import dao.EscuelasOficialesIdiomasDAO;
import dao.EoiDAO;
import dao.IdiomaDAO;
import dao.NivelDAO;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import modelo.Curso;
import modelo.Eoi;
import modelo.EscuelasOficialesIdiomas;
import modelo.Idioma;
import modelo.Nivel;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author Sandra
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException, SQLException, Exception {
        Scanner teclado = new Scanner(System.in);

        int opcion, codigo, codMunicipio, codProvincia, resultado, codCurso, numero;
        String nomMunicipio, nomProvincia, descripcion;
        char codNivel;

        File origen = new File("alumnos.xml");
        File destino = new File("alumnosCopia.xml");

        Document d = null;

        ControlDom cd = new ControlDom();
        ControlEscuelasOficialesIdiomas controlEscuelas = null;

        Connection con = null;

        ArrayList<Nivel> listaNiveles;
        ArrayList<Curso> listaCursos;
        ArrayList<Idioma> listaIdiomas;

        EscuelasOficialesIdiomasDAO escuelasOficialesIdiomasDAO = new EscuelasOficialesIdiomasDAO();
        EoiDAO eoiDAO = new EoiDAO();
        CursoDAO cursoDAO = new CursoDAO();
        NivelDAO nivelDAO = new NivelDAO();
        IdiomaDAO idiomaDAO = new IdiomaDAO();

        Eoi eoi;
        Nivel n;
        Curso c;
        Idioma i;

        //ESTE OBJETO ES UN ARRAYLIST
        EscuelasOficialesIdiomas escuelas = null;
        EscuelasOficialesIdiomas escuelas2;

        destino.createNewFile();

        do {

            System.out.println("Introduce opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1:
                    //ABRIR CONEXIÓN
                    System.out.println("ABRIENDO CONEXIÓN...");
                    con = escuelasOficialesIdiomasDAO.AbrirConexion();
                    System.out.println("CONEXIÓN ABIERTA.");
                    break;
                case 2:
                    //RECUPERAR DEL XML Y CREAR EL DOCUMENT

                    controlEscuelas = new ControlEscuelasOficialesIdiomas(origen);
                    d = controlEscuelas.recuperar();
                    System.out.println("Document (árbol de nodos) creado con éxito");
                    break;
                case 3:
                    //LEER DEL DOCUMENT Y CREAR EL OBJETO
                    escuelas = controlEscuelas.leer(d);
                    System.out.println(escuelas);
                    break;
                case 4:
                    //ESCRIBIR EL DOCUMENT A PARTIR DEL OBJETO ESCUELAS OFICIALES IDIOMAS

                    //CREAMOS UN NUEVO OBJETO ESCUELAS OFICIALES IDIOMAS PASÁNDOLE EL FILE DESTINO
                    controlEscuelas = new ControlEscuelasOficialesIdiomas(destino, escuelas);

                    //CREAMOS EL DOCUMENT VACÍO
                    d = cd.deXmlaDocumento();

                    //LO RELLENAMOS CON LO QUE HEMOS HECHO EN EL ESCRIBIR
                    controlEscuelas.escribir(d);

                    System.out.println("Document (árbol de nodos) creado con éxito a partir del objeto Escuelas Oficiales Idiomas");
                    break;
                case 5:
                    //GUARDAR EL DOCUMENT EN XML
                    controlEscuelas.guardar(d);
                    System.out.println("Document (árbol de nodos) guardado exitosamente en archivo XML");
                    break;
                case 6:
                    //INSERTAR TODOS LOS DATOS EN LA BD
                    insertarEoi(escuelas, con, eoiDAO);
                    System.out.println("Datos insertados con éxito");
                    break;
                case 7:
                    //ELIMINAR

                    System.out.println("Elige qué elemento quieres eliminar");
                    mostrarElementos();
                    opcion = teclado.nextInt();
                    switch (opcion) {
                        case 1:
                            //ELIMINAR ESCUELA ENTERA
                            //MOSTRAR LAS ESCUELAS
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);
                            System.out.println("Introduce el código del EOI a eliminar");
                            codigo = teclado.nextInt();

                            eoi = new Eoi();
                            eoi.setCodigo(codigo);
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTE ES EL EOI ELEGIDO " + eoi);

                            resultado = eoiDAO.elimina(con, eoi);
                            if (resultado != 0) {
                                System.out.println("Eoi eliminada con éxito");
                            }
                            System.out.println(escuelas);
                            break;
                        case 2:
                            //ELIMINAR NIVEL
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);

                            System.out.println("Introduce el código del EOI donde está el nivel a eliminar");
                            codigo = teclado.nextInt();

                            eoi = new Eoi();
                            eoi.setCodigo(codigo);
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTE ES EL EOI ELEGIDO " + eoi);

                            System.out.println("Introduce el id del nivel a eliminar");
                            codigo = teclado.nextInt();
                            n = new Nivel();
                            n.setId(codigo);
                            resultado = nivelDAO.elimina(con, n);
                            if (resultado != 0) {
                                System.out.println("Nivel eliminado con éxito");
                            }
                            System.out.println(eoi);
                            break;
                        case 3:
                            //ELIMINAR CURSO
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);

                            System.out.println("Introduce el código del EOI donde está el curso a eliminar");
                            codigo = teclado.nextInt();

                            eoi = new Eoi();
                            eoi.setCodigo(codigo);
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);

                            System.out.println("ESTE ES EL EOI ELEGIDO " + eoi);

                            System.out.println("Introduce el id del curso a eliminar");
                            codigo = teclado.nextInt();
                            c = new Curso();
                            c.setId(codigo);
                            resultado = cursoDAO.elimina(con, c);
                            if (resultado != 0) {
                                System.out.println("Curso eliminado con éxito");
                            }
                            System.out.println(eoi);
                            break;
                        case 4:
                            //ELIMINAR IDIOMA
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);

                            System.out.println("Introduce el código del EOI donde está el idioma a eliminar");
                            codigo = teclado.nextInt();

                            eoi = new Eoi();
                            eoi.setCodigo(codigo);
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTE ES EL EOI ELEGIDO " + eoi);

                            System.out.println("Introduce el id del idioma a eliminar");
                            codigo = teclado.nextInt();
                            i = new Idioma();
                            i.setId(codigo);
                            resultado = idiomaDAO.elimina(con, i);
                            if (resultado != 0) {
                                System.out.println("Idioma eliminado con éxito");
                            }
                            System.out.println(eoi);
                            break;
                        case 5:
                            System.out.println("Volviendo...");
                            break;
                        default:
                            throw new AssertionError();
                    }
                    break;

                case 8:
                    //ACTUALIZAR
                    System.out.println("Elige un elemento a actualizar");
                    mostrarElementos();
                    opcion = teclado.nextInt();

                    //----------------------------------------------ACTUALIZA ESCUELAS------------------------------------
                    switch (opcion) {
                        case 1:
                            //ACTUALIZAR ESCUELA
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);
                            System.out.println("Introduce el código de la escuela que quieres actualizar");
                            codigo = teclado.nextInt();
                            eoi = new Eoi();
                            eoi.setCodigo(codigo);

                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTA ES LA ESCUELA ELEGIDA " + eoi);

                            System.out.println("Elige opción");
                            menuActualizarEscuela();
                            opcion = teclado.nextInt();

                            switch (opcion) {
                                case 1:
                                    //ACTUALIZAR CÓDIGO PROVINCIA
                                    System.out.println("Elige el nuevo código de la provincia");
                                    codProvincia = teclado.nextInt();
                                    eoi.setCodProvincia(codProvincia);

                                    break;
                                case 2:
                                    //ACTUALIZAR NOMBRE PROVINCIA
                                    System.out.println("Elige el nuevo nombre de la provincia");
                                    teclado.nextLine();
                                    nomProvincia = teclado.nextLine();
                                    eoi.setNomProvincia(nomProvincia);
                                    break;
                                case 3:
                                    //ACTUALIZAR CÓDIGO MUNICIPIO
                                    System.out.println("Elige el nuevo código del municipio");
                                    codMunicipio = teclado.nextInt();
                                    eoi.setCodMunicipio(codMunicipio);
                                    break;
                                case 4:
                                    //ACTUALIZAR NOMBRE MUNICIPIO
                                    teclado.nextLine();
                                    System.out.println("Elige el nuevo nombre del municipio");
                                    nomMunicipio = teclado.nextLine();
                                    eoi.setNomMunicipio(nomMunicipio);
                                    break;

                                case 5:
                                    System.out.println("Volviendo..");
                                    break;
                                default:
                                    System.out.println("Opción inválida");
                            }
                            eoiDAO.actualiza(con, eoi);
                            System.out.println("EOI actualizada con éxito");
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println(eoi);
                            break;

                        //---------------------------------------------- ACTUALIZA NIVELES------------------------------------
                        case 2:
                            //ACTUALIZAR NIVEL
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);
                            System.out.println("Introduce el código de la ESCUELA que contiene el nivel que quieres actualizar");
                            codigo = teclado.nextInt();
                            eoi = new Eoi();
                            eoi.setCodigo(codigo);

                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTA ES LA ESCUELA ELEGIDA " + eoi);

                            System.out.println("Introduce el id del nivel que quieres actualizar");
                            codigo = teclado.nextInt();
                            n = new Nivel();
                            n.setId(codigo);

                            n = nivelDAO.obtenerNivelPorId(con, n);
                            System.out.println("ESTE ES EL NIVEL ELEGIDO " + n);
                            System.out.println("Elige opción");
                            menuActualizarNivel();
                            opcion = teclado.nextInt();
                            switch (opcion) {
                                case 1:
                                    //ACTUALIZAR CÓDIGO
                                    System.out.println("Elige el nuevo código del nivel (RECUERDA QUE ES UN SOLO CARÁCTER)");
                                    codNivel = teclado.next().charAt(0);
                                    n.setCodigo(codNivel);
                                    break;
                                case 2:
                                    //ACTUALIZAR DESCRIPCIÓN
                                    System.out.println("Elige la nueva descripción del nivel");
                                    teclado.nextLine();
                                    descripcion = teclado.nextLine();
                                    n.setDescripcion(descripcion);
                                    break;

                                case 3:
                                    System.out.println("Volviendo..");
                                    break;
                                default:
                                    System.out.println("Opción inválida");
                            }
                            nivelDAO.actualiza(con, n);
                            System.out.println("Nivel actualizado con éxito");
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println(eoi);
                            break;

                        //---------------------------------------------- ACTUALIZA CURSOS------------------------------------
                        case 3:
                            //ACTUALIZAR CURSO
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);
                            System.out.println("Introduce el código de la escuela que contiene el curso que quieres actualizar");
                            codigo = teclado.nextInt();
                            eoi = new Eoi();
                            eoi.setCodigo(codigo);

                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTA ES LA ESCUELA ELEGIDA " + eoi);

                            System.out.println("Introduce el id del curso que quieres actualizar");
                            codigo = teclado.nextInt();
                            c = new Curso();
                            c.setId(codigo);

                            c = cursoDAO.obtenerCursoPorId(con, c);
                            System.out.println("ESTE ES EL CURSO ELEGIDO " + c);

                            System.out.println("Elige opción");
                            menuActualizarCurso();
                            opcion = teclado.nextInt();
                            switch (opcion) {
                                case 1:
                                    //ACTUALIZAR CÓDIGO
                                    System.out.println("Elige el nuevo código del curso");
                                    codCurso = teclado.nextInt();
                                    c.setCodigo(codCurso);
                                    break;
                                case 2:
                                    System.out.println("Volviendo..");
                                    break;
                                default:
                                    System.out.println("Opción inválida");
                            }
                            cursoDAO.actualiza(con, c);
                            System.out.println("Curso actualizado con éxito");

                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println(eoi);
                            break;

                        //---------------------------------------------- ACTUALIZA IDIOMAS------------------------------------
                        case 4:
                            //ACTUALIZAR IDIOMA
                            escuelas = eoiDAO.obtenerEois(con);
                            System.out.println("MOSTRANDO LISTA DE ESCUELAS...");
                            System.out.println(escuelas);
                            System.out.println("Introduce el código de la escuela que contiene el nivel que quieres actualizar");
                            codigo = teclado.nextInt();
                            eoi = new Eoi();
                            eoi.setCodigo(codigo);

                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println("ESTA ES LA ESCUELA ELEGIDA " + eoi);

                            System.out.println("Introduce el id del idioma que quieres actualizar");
                            codigo = teclado.nextInt();
                            i = new Idioma();
                            i.setId(codigo);

                            i = idiomaDAO.obtenerIdiomaPorId(con, i);
                            System.out.println("ESTE ES EL IDIOMA ELEGIDO " + i);

                            System.out.println("Elige opción");
                            menuActualizarIdioma();
                            opcion = teclado.nextInt();
                            switch (opcion) {
                                case 1://ACTUALIZAR DESCRIPCIÓN
                                    System.out.println("Elige la nueva descripción del idioma");
                                    teclado.nextLine();
                                    descripcion = teclado.nextLine();
                                    i.setDescripcion(descripcion);
                                    break;
                                case 2://ACTUALIZAR GRUPOS
                                    System.out.println("Elige el nuevo número de grupos que hay");
                                    numero = teclado.nextInt();
                                    i.setGrupos(numero);
                                    break;
                                case 3://ACTUALIZAR TOTAL ALUMNOS
                                    System.out.println("Elige el nuevo número total de alumnos que hay");
                                    numero = teclado.nextInt();
                                    i.setTotalAlumnos(numero);
                                    break;
                                case 4:
                                    System.out.println("Volviendo..");
                                    break;
                                default:
                                    System.out.println("Opción inválida");
                            }
                            idiomaDAO.actualiza(con, i);
                            System.out.println("Idioma actualizado con éxito");
                            eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                            System.out.println(eoi);
                            break;

                        default:
                            System.out.println("Opción inválida");
                    }
                    break;
                case 9://MOSTRAR TODA LA BASE DE DATOS (FIND ALL)
                    escuelas = eoiDAO.obtenerEois(con);
                    System.out.println(escuelas);
                    break;
                case 10://BUSCAR ESCUELAS POR ID 
                    escuelas = eoiDAO.obtenerEois(con);
                    System.out.println(escuelas);
                    System.out.println("Introduce el codigo de la escuela que buscas");
                    codigo = teclado.nextInt();
                    eoi = new Eoi();
                    eoi.setCodigo(codigo);
                    eoi = eoiDAO.obtenerEoiPorId(con, eoi);
                    System.out.println("LA ESCUELA BUSCADA ES " + eoi);
                    break;
                case 11:
                    //BUSCAR ESCUELAS POR PROVINCIA
                    System.out.println("Introduce el número de la provincia de la Comunidad Valenciana donde quieras buscar escuelas");
                    menuProvincias();
                    opcion = teclado.nextInt();
                    switch (opcion) {
                        case 1://CASTELLÓN
                            escuelas = eoiDAO.buscarEscuelasPorProvincia(con, "CASTELLÓN DE LA PLANA");
                            System.out.println("EN CASTELLÓN HAY " +escuelas.size() + " ESCUELAS");
                            System.out.println(escuelas);
                            break;
                        case 2: //VALENCIA
                            escuelas=eoiDAO.buscarEscuelasPorProvincia(con, "VALENCIA");
                         System.out.println("EN VALENCIA HAY " +escuelas.size() + " ESCUELAS");
                            System.out.println(escuelas);
                            break;
                        case 3://ALICANTE
                            escuelas=eoiDAO.buscarEscuelasPorProvincia(con, "ALICANTE");
                           System.out.println("EN ALICANTE HAY " +escuelas.size() + " ESCUELAS");
                            System.out.println(escuelas);
                            break;
                        case 4:
                            System.out.println("Saliendo...");
                            break;
                        default:
                            System.out.println("Opción inválida");
                    }
                    break;
                case 12:
                    //CERRAR CONEXIÓN
                    System.out.println("CONEXIÓN CERRADA");
                    escuelasOficialesIdiomasDAO.CerrarConexion(con);
                    break;
                case 13:
                    System.out.println("Saliendo...");
                    break;

                default:
                    System.out.println("Opción incorrecta");
            }
        } while (opcion != 13);
    }

    public static void menu() {
        System.out.println("1. ABRIR CONEXIÓN CON BASE DE DATOS");
        System.out.println("2. Recuperar del xml y crear el document");
        System.out.println("3. Leer del document y crear el objeto ");
        System.out.println("4. Escribir el document a partir del objeto");
        System.out.println("5. Guardar en xml el document");

        System.out.println("6. CARGA MASIVA DE TODOS LOS DATOS DEL XML EN LA BD (SOLO EJECUTAR UNA VEZ) ATENCION!!!!!!TARDA UN POCO EN CARGAR!!!! HACER ANTES EL CASE 2 DONDE SE CREA EL OBJETO");
        System.out.println("7. Eliminar datos BD");
        System.out.println("8. Actualizar datos");
        System.out.println("9. Mostrar toda la base de datos");
        System.out.println("10. Buscar escuelas por id");
        System.out.println("11. Buscar escuelas por provincia ");
        System.out.println("12. CERRAR CONEXTIÓN CON BASE DE DATOS");
        System.out.println("13. Salir");
    }

    public static void insertarEoi(ArrayList<Eoi> listaEois, Connection con, EoiDAO eoiDAO) throws SQLException {
        for (Eoi eoi : listaEois) {
            eoiDAO.inserta(con, eoi);
        }
    }

    public static void mostrarElementos() {
        System.out.println("1. Una escuela");
        System.out.println("2. Un nivel");
        System.out.println("3. Un curso");
        System.out.println("4. Un idioma");
        System.out.println("5. Atrás");
    }

    public static void menuActualizarEscuela() {
        System.out.println("1. Actualizar código provincia");
        System.out.println("2. Actualizar nombre provincia");
        System.out.println("3. Actualizar código municipio");
        System.out.println("4. Actualizar nombre municipio");
        System.out.println("5. Atrás");
    }

    public static void menuActualizarNivel() {
        System.out.println("1. Actualizar código");
        System.out.println("2. Actualizar descripción");
        System.out.println("3. Atrás");
    }

    public static void menuActualizarCurso() {
        System.out.println("1. Actualizar código");
        System.out.println("2. Atrás");
    }

    public static void menuActualizarIdioma() {
        System.out.println("1. Actualizar descripción");
        System.out.println("2. Actualizar grupos");
        System.out.println("3. Actualizar total alumnos");
        System.out.println("4. Atrás");
    }

    public static void menuProvincias() {
        System.out.println("1. Castellón");
        System.out.println("2. Valencia");
        System.out.println("3. Alicante");
        System.out.println("4. Atrás");

    }
}
