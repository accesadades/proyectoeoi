/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Curso;
import modelo.Eoi;
import modelo.Idioma;
import modelo.Nivel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Sandra
 */
public class ControlEoi extends ControlDom {

    static final String EOI = "EOI";
    static final String EOI_CODIGO = "EOI_CODIGO";
    static final String COD_PROVINCIA = "COD_PROVINCIA";
    static final String NOM_PROVINCIA = "NOM_PROVINCIA";
    static final String COD_MUNICIPIO = "COD_MUNICIPIO";
    static final String NOM_MUNICIPIO = "NOM_MUNICIPIO";
    static final String NIVELES = "NIVELES";
    static final String NIVEL = "NIVEL";
    static final String CURSOS = "CURSOS";
    static final String CURSO = "CURSO";
    static final String CODIGO = "CODIGO";
    static final String IDIOMAS = "IDIOMAS";
    static final String IDIOMA = "IDIOMA";
    static final String DESCRIPCION = "DESCRIPCION";
    static final String GRUPOS = "GRUPOS";
    static final String TOTAL_ALUMNOS = "TOTAL_ALUMNOS";
    static int idCurso=0;
    static int idNivel=0;
    static int idIdioma=0;

    public static Eoi leerEoi(Element escuela) {

        Element codigo, codProvincia, nomProvincia, codMunicipio, nomMunicipio, niveles;
        String textoNomProvincia, textoNomMunicipio;
        int numCodigo, numCodProvincia, numCodMunicipio;
        NodeList listaNiveles;
        Nivel n;
        Eoi objetoEscuela;

        codigo = obtenerElementoEtiqueta(EOI_CODIGO, escuela);
        codProvincia = obtenerElementoEtiqueta(COD_PROVINCIA, escuela);
        nomProvincia = obtenerElementoEtiqueta(NOM_PROVINCIA, escuela);
        codMunicipio = obtenerElementoEtiqueta(COD_MUNICIPIO, escuela);
        nomMunicipio = obtenerElementoEtiqueta(NOM_MUNICIPIO, escuela);
        niveles = obtenerElementoEtiqueta(NIVELES, escuela);

        numCodigo = Integer.parseInt(obtenerTextoEtiqueta(EOI_CODIGO, escuela));
        numCodProvincia = Integer.parseInt(obtenerTextoEtiqueta(COD_PROVINCIA, escuela));
        textoNomProvincia = obtenerTextoEtiqueta(NOM_PROVINCIA, escuela);
        numCodMunicipio = Integer.parseInt(obtenerTextoEtiqueta(COD_MUNICIPIO, escuela));
        textoNomMunicipio = obtenerTextoEtiqueta(NOM_MUNICIPIO, escuela);

        objetoEscuela = new Eoi(numCodigo, numCodProvincia, textoNomProvincia, numCodMunicipio, textoNomMunicipio);

        listaNiveles = niveles.getChildNodes();

        for (int i = 0; i < listaNiveles.getLength(); i++) {
            if (listaNiveles.item(i).getNodeType() == Node.ELEMENT_NODE) {
                n = leerNivel((Element) listaNiveles.item(i));
                objetoEscuela.getNiveles().add(n);
            }
        }
        return objetoEscuela;
    }

    public static Nivel leerNivel(Element nivel) {

        Element codigo, descripcion, cursos;
        NodeList listaCursos;
        char letraCodigo;
        String textoDescripcion;
        Curso c;
        Nivel n;
        

        codigo = obtenerElementoEtiqueta(CODIGO, nivel);
        descripcion = obtenerElementoEtiqueta(DESCRIPCION, nivel);
        cursos = obtenerElementoEtiqueta(CURSOS, nivel);

        letraCodigo = (obtenerTextoEtiqueta(CODIGO, nivel)).charAt(0);
        textoDescripcion = obtenerTextoEtiqueta(DESCRIPCION, nivel);

        listaCursos = cursos.getChildNodes();

        idNivel++;
        n = new Nivel(letraCodigo, textoDescripcion,idNivel);

        for (int i = 0; i < listaCursos.getLength(); i++) {
            if (listaCursos.item(i).getNodeType() == Node.ELEMENT_NODE) {
                c = leerCurso((Element) listaCursos.item(i));
                n.getCursos().add(c);
            }
        }
        return n;
    }

    public static Curso leerCurso(Element curso) {

        Element codigo, idiomas;
        NodeList listaIdiomas;
        int numCodigo;
        Idioma i;
        Curso c;

        codigo = obtenerElementoEtiqueta(CODIGO, curso);
        idiomas = obtenerElementoEtiqueta(IDIOMAS, curso);

        numCodigo = Integer.parseInt(obtenerTextoEtiqueta(CODIGO, curso));

        listaIdiomas = idiomas.getChildNodes();

        idCurso++;
        c = new Curso(numCodigo, idCurso);

        for (int j = 0; j < listaIdiomas.getLength(); j++) {
            if (listaIdiomas.item(j).getNodeType() == Node.ELEMENT_NODE) {
                i = leerIdioma((Element) listaIdiomas.item(j));
                c.getIdiomas().add(i);
            }
        }
        return c;
    }

    public static Idioma leerIdioma(Element idioma) {

        Element descripcion, grupos, totalAlumnos;
        String textoDescripcion;
        int numGrupos, numTotalAlumnos;
        Idioma i;

        descripcion = obtenerElementoEtiqueta(DESCRIPCION, idioma);
        grupos = obtenerElementoEtiqueta(GRUPOS, idioma);
        totalAlumnos = obtenerElementoEtiqueta(TOTAL_ALUMNOS, idioma);

        textoDescripcion = obtenerTextoEtiqueta(DESCRIPCION, idioma);
        numGrupos = Integer.parseInt(obtenerTextoEtiqueta(GRUPOS, idioma));
        numTotalAlumnos = Integer.parseInt(obtenerTextoEtiqueta(TOTAL_ALUMNOS, idioma));

        idIdioma++;
        i = new Idioma(textoDescripcion, numGrupos, numTotalAlumnos,idIdioma);
        return i;
    }

    public static Element escribir(Eoi escuela, Element escuelas, Document d) {
        
        //CREAMOS LOS ELEMENTS
        Element elemCodigo = d.createElement(EOI_CODIGO);
        Element elemCodMun = d.createElement(COD_MUNICIPIO);
        Element elemCodProv = d.createElement(COD_PROVINCIA);
        Element elemNomMun = d.createElement(NOM_MUNICIPIO);
        Element elemNomProv = d.createElement(NOM_PROVINCIA);
        Element elemEscuela = d.createElement(EOI);
        Element elemNiveles = d.createElement(NIVELES);
        
        //RECUPERAMOS LOS DATOS DEL ARRAY
        int codigo = escuela.getCodigo();
        int codMunicipio = escuela.getCodMunicipio();
        int codProvincia = escuela.getCodProvincia();
        String nomMunicipio = escuela.getNomMunicipio();
        String nomProvincia = escuela.getNomProvincia();

        //CREAMOS LOS NODOS DE TEXTO
        Node cod = d.createTextNode(String.valueOf(codigo));
        Node codMun = d.createTextNode(String.valueOf(codMunicipio));
        Node codProv = d.createTextNode(String.valueOf(codProvincia));
        Node nomMun = d.createTextNode(nomMunicipio);
        Node nomProv = d.createTextNode(nomProvincia);

        //AÑADIMOS LOS NODOS DE TEXTO A LOS ELEMENTS
        elemCodigo.appendChild(cod);
        elemCodMun.appendChild(codMun);
        elemCodProv.appendChild(codProv);
        elemNomMun.appendChild(nomMun);
        elemNomProv.appendChild(nomProv);

        //TRABAJAMOS CON EL ARRAY DE NIVELES
        for (int i = 0; i < escuela.getNiveles().size(); i++) {
           escribirNivel(escuela.getNiveles().get(i), d, elemNiveles); 
        }
        //AÑADIMOS ESCUELA A ESCUELAS
        escuelas.appendChild(elemEscuela);
        
        //AÑADIMOS LOS ELEMENTS AL ELEMENT PADRE (ESCUELA)
        elemEscuela.appendChild(elemCodigo);
        elemEscuela.appendChild(elemCodMun);
        elemEscuela.appendChild(elemCodProv);
        elemEscuela.appendChild(elemNomMun);
        elemEscuela.appendChild(elemNomProv);
        elemEscuela.appendChild(elemNiveles);
        
        
        return elemEscuela;
    }

    public static Element escribirNivel(Nivel n, Document d, Element elemNiveles) {

        char codigo=n.getCodigo();
        String descripcion=n.getDescripcion();
        
        Node cod=d.createTextNode(Character.toString(codigo));
        Node desc=d.createTextNode(descripcion);
        
        Element elemCodigo = d.createElement(CODIGO);
        Element elemDescripcion = d.createElement(DESCRIPCION);
        Element elemNivel=d.createElement(NIVEL);
        Element elemCursos=d.createElement(CURSOS);
        
        elemCodigo.appendChild(cod);
        elemDescripcion.appendChild(desc);
        
        elemNivel.appendChild(elemCodigo);
        elemNivel.appendChild(elemDescripcion);
        elemNivel.appendChild(elemCursos);
        elemNiveles.appendChild(elemNivel);
        
        for (int i = 0; i < n.getCursos().size(); i++) {
            escribirCurso(n.getCursos().get(i),d,elemCursos);
        }
        
        return elemNivel;
    }
    public static Element escribirCurso(Curso c, Document d, Element elemCursos){
        
        int codigo=c.getCodigo();
        
        Node cod=d.createTextNode(Integer.toString(codigo));
        
        Element elemCodigo=d.createElement(CODIGO);
        Element elemCurso=d.createElement(CURSO);
        Element elemIdiomas=d.createElement(IDIOMAS);
        
        elemCodigo.appendChild(cod);
        
        elemCurso.appendChild(elemCodigo);
        elemCurso.appendChild(elemIdiomas);
        elemCursos.appendChild(elemCurso);
        
        for (int i = 0; i < c.getIdiomas().size(); i++) {
          escribirIdioma(c.getIdiomas().get(i), d, elemIdiomas);
        }
        
        return elemCurso;
    }
    
    public static Element escribirIdioma(Idioma i, Document d, Element elemIdiomas){
        
        String descripcion=i.getDescripcion();
        int grupos=i.getGrupos();
        int totalAlumnos=i.getTotalAlumnos();
        
        Node desc= d.createTextNode(descripcion);
        Node grup=d.createTextNode(Integer.toString(grupos));
        Node total=d.createTextNode(Integer.toString(totalAlumnos));
        
        Element elemDescripcion=d.createElement(DESCRIPCION);
        Element elemGrupos=d.createElement(GRUPOS);
        Element elemTotalAlumnos = d.createElement(TOTAL_ALUMNOS);
        Element elemIdioma=d.createElement(IDIOMA);
        
        elemDescripcion.appendChild(desc);
        elemGrupos.appendChild(grup);
        elemTotalAlumnos.appendChild(total);    
        
        elemIdioma.appendChild(elemDescripcion);
        elemIdioma.appendChild(elemGrupos);
        elemIdioma.appendChild(elemTotalAlumnos);
        
        elemIdiomas.appendChild(elemIdioma);
        
        return elemIdioma;
    }
}
