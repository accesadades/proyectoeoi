/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import modelo.Eoi;
import modelo.EscuelasOficialesIdiomas;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import modelo.Eoi;
import modelo.EscuelasOficialesIdiomas;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Sandra
 */


public class ControlEscuelasOficialesIdiomas extends ControlDom {

    static final String ESCUELAS_OFICIALES_IDIOMAS = "ESCUELAS_OFICIALES_IDIOMAS";
    private File f;
    EscuelasOficialesIdiomas escuelas;

    //UN CONSTRUCTOR AMB EL FILE QUE NECESSITE PER A CRIDAR A RECUPERAR
    public ControlEscuelasOficialesIdiomas(File f) {
        this.f = f;
        this.escuelas=new EscuelasOficialesIdiomas();
    }
    //ESTE CONSTRUCTOR LO USAREMOS PARA ESCRIBIR PASÁNDOLE EL FILE DESTINO Y EL ARRAYLIST ESCUELAS QUE NOS HEMOS CREADO EN EL CASE 2
    public ControlEscuelasOficialesIdiomas(File f, EscuelasOficialesIdiomas escuelas) {
        this.f = f;
        this.escuelas=escuelas;
    }
    
    public Document recuperar() throws ParserConfigurationException, SAXException, IOException {
        Document d;
        d = deXmlaDocumento(this.f);
        d.normalize();
        return d;
    }

    public EscuelasOficialesIdiomas leer(Document d) {

        //obtenemos el elemento raiz
        Element ElementoEscuelas = d.getDocumentElement();
        NodeList listaEscuelas;
        Eoi escuela;
       
        //obtenemos los hijos eoi
        listaEscuelas = ElementoEscuelas.getChildNodes();

        for (int i = 0; i < listaEscuelas.getLength(); i++) {
            if (listaEscuelas.item(i).getNodeType() == Node.ELEMENT_NODE) {
                escuela = ControlEoi.leerEoi((Element) listaEscuelas.item(i));
                this.escuelas.add(escuela);
            }
        }
        return this.escuelas;
    }

    public void escribir(Document d) {

        Element elemEscuelas = d.createElement(ESCUELAS_OFICIALES_IDIOMAS);
        Element elemEscuela = null;
        
        d.appendChild(elemEscuelas);
        
        for (Eoi escuela : this.escuelas) {
          ControlEoi.escribir(escuela, elemEscuelas, d);
        }
    }

    public void guardar(Document d) throws TransformerException {
            ControlDom cd= new ControlDom();
            cd.deDocaXml(d, this.f);
    }

    public File getF() {
        return f;
    }

    public void setF(File f) {
        this.f = f;
    }

    public EscuelasOficialesIdiomas getEscuelas() {
        return escuelas;
    }

}
