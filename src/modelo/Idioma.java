/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class Idioma {
    private String descripcion;
    private int grupos;
    private int totalAlumnos;
    private int id;

    public Idioma(String descripcion, int grupos, int totalAlumnos, int id) {
        this.descripcion = descripcion;
        this.grupos = grupos;
        this.totalAlumnos = totalAlumnos;
        this.id=id;
    }
    public Idioma(){
        
    }
    

    @Override
    public String toString() {
        return "\n\t\t\tIDIOMA: ID " + id+ " DESCRIPCIÓN: " + descripcion + ", GRUPOS: " + grupos + ", TOTAL ALUMNOS: " + totalAlumnos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getGrupos() {
        return grupos;
    }

    public void setGrupos(int grupos) {
        this.grupos = grupos;
    }

    public int getTotalAlumnos() {
        return totalAlumnos;
    }

    public void setTotalAlumnos(int totalAlumnos) {
        this.totalAlumnos = totalAlumnos;
    } 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
