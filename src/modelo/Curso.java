/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Sandra
 */
public class Curso {
    
    private int codigo;
    private int id;
    ArrayList<Idioma> idiomas;

    public Curso(int codigo, int id) {
       
        this.codigo = codigo;
        this.idiomas = new ArrayList<Idioma>();
        this.id=id;
    }
    public Curso(){
        
    }

    @Override
    public String toString() {
         String texto="\n\t\tCURSO: ID " + this.id+ " CÓDIGO: "+this.codigo;
        for (Idioma i : this.idiomas) {
            texto+= i.toString(); 
        }
      return texto;
    }
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public ArrayList<Idioma> getIdiomas() {
        return idiomas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdiomas(ArrayList<Idioma> idiomas) {
        this.idiomas = idiomas;
    }
    
    
}
