/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

public class Nivel {
    
    private char codigo;
    private String descripcion;
    ArrayList<Curso> cursos;
    private int id;
   

    public Nivel(char codigo, String descripcion, int id) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.cursos= new ArrayList<Curso>();
      this.id=id;
    }
    
    public Nivel(){
        
    }
    @Override
    public String toString() {
          String texto="\n\tNIVEL: ID " + id+ " CÓDIGO: " + codigo + ", DESCRIPCION: " + descripcion;
        for (Curso c : this.cursos) {
            texto+= c.toString(); 
        }
      return texto;
     
    }
    public ArrayList<Curso> getCursos() {
        return cursos;
    }

    public char getCodigo() {
        return codigo;
    }

    public void setCodigo(char codigo) {
        this.codigo = codigo;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    } 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCursos(ArrayList<Curso> cursos) {
        this.cursos = cursos;
    }

}
