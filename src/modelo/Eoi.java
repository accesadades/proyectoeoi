/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Sandra
 */
public class Eoi {
    
    private int codigo;
    private int codProvincia;
    private String nomProvincia;
    private int codMunicipio;
    private String nomMunicipio;
    ArrayList<Nivel> niveles;

    public Eoi(int eoiCodigo, int codProvincia, String nomProvincia, int codMunicipio, String nomMunicipio) {
        
        this.codigo = eoiCodigo;
        this.codProvincia = codProvincia;
        this.nomProvincia = nomProvincia;
        this.codMunicipio = codMunicipio;
        this.nomMunicipio = nomMunicipio;
        this.niveles= new ArrayList<Nivel>();
    }
    
    public Eoi(){
        
    }
    
    @Override
    public String toString() {
          String texto="\nEOI: " + "CÓDIGO ESCUELA: " + codigo + ", CÓDIGO PROVINCIA: " + codProvincia +", NOMBRE PROVINCIA: "
                  + nomProvincia + ", CÓDIGO MUNICIPIO: " + codMunicipio + ", NOMBRE MUNICIPIO: " + nomMunicipio;
        for (Nivel n : this.niveles) {
            texto+= n.toString(); 
        }
      return texto;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodProvincia() {
        return codProvincia;
    }

    public void setCodProvincia(int codProvincia) {
        this.codProvincia = codProvincia;
    }

    public String getNomProvincia() {
        return nomProvincia;
    }

    public void setNomProvincia(String nomProvincia) {
        this.nomProvincia = nomProvincia;
    }

    public int getCodMunicipio() {
        return codMunicipio;
    }

    public void setCodMunicipio(int codMunicipio) {
        this.codMunicipio = codMunicipio;
    }

    public String getNomMunicipio() {
        return nomMunicipio;
    }

    public void setNomMunicipio(String nomMunicipio) {
        this.nomMunicipio = nomMunicipio;
    }

    public ArrayList<Nivel> getNiveles() {
        return niveles;
    }  

    public void setNiveles(ArrayList<Nivel> niveles) {
        this.niveles = niveles;
    }

    
    
}
