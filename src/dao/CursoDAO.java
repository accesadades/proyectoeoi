/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Curso;
import modelo.Eoi;
import modelo.Idioma;
import modelo.Nivel;

/**
 *
 * @author Sandra
 */
public class CursoDAO {

    private IdiomaDAO idiomaDAO;

    public CursoDAO() {

        this.idiomaDAO = new IdiomaDAO();
    }

    public void insertaCursos(Curso c, Connection con, Nivel n) throws SQLException {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO curso (id,codigo,id_nivel) VALUES(?,?,?)");
            stmt.setInt(1, c.getId());
            stmt.setInt(2, c.getCodigo());
            stmt.setInt(3, n.getId());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        for (Idioma i : c.getIdiomas()) {
            this.idiomaDAO.insertaIdiomas(i, con, c);
        }

    }

    public int elimina(Connection con, Curso c) throws SQLException {
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement("DELETE FROM curso WHERE id=?");
            stmt.setInt(1, c.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR, ESTÁS INTENTANDO ELIMINAR UN OBJETO QUE CONTIENE OBJETOS DENTRO, POR FAVOR, ELIMINA PRIMERO LOS OBJETOS DEL INTERIOR");
            return 0;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return 1;
    }

    public void actualiza(Connection con, Curso c) throws SQLException {
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement("UPDATE curso SET codigo=? WHERE id=?");

            stmt.setInt(1, c.getCodigo());
            stmt.setInt(2, c.getId());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public ArrayList<Curso> obtenerCursos(Connection con, int claveAjena) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Curso c;
        int codigo, id;
        ArrayList<Curso> listaCursos = new ArrayList<Curso>();
        ArrayList<Idioma> listaIdiomas = new ArrayList<Idioma>();
        IdiomaDAO idiomaDAO = new IdiomaDAO();

        try {
            stmt = con.prepareStatement("SELECT * FROM curso WHERE id_nivel=?");

            stmt.setInt(1, claveAjena);
            rs = stmt.executeQuery();

            while (rs.next()) {
                id = rs.getInt("id");
                codigo = rs.getInt("codigo");
                c = new Curso(codigo, id);
                
                listaIdiomas = idiomaDAO.obtenerIdiomas(con, id);
                c.setIdiomas(listaIdiomas);
                listaCursos.add(c);

            }
        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return listaCursos;
    }

    public Curso obtenerCursoPorId(Connection con, Curso c) throws SQLException {

        ArrayList<Idioma> listaIdiomas;
        IdiomaDAO idiomaDAO= new IdiomaDAO();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM curso WHERE id=?");

            stmt.setInt(1, c.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
                
                c.setCodigo(rs.getInt("codigo"));
                
                listaIdiomas=idiomaDAO.obtenerIdiomas(con, c.getId());
                c.setIdiomas(listaIdiomas);
            }
            

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return c;
    }

}
