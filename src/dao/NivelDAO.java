/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Curso;
import modelo.Eoi;
import modelo.Nivel;

/**
 *
 * @author Sandra
 */
public class NivelDAO {

    private CursoDAO cursoDAO;

    public NivelDAO() {
        this.cursoDAO = new CursoDAO();
    }

    public void insertaNiveles(Nivel n, Connection con, Eoi eoi) throws SQLException {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO nivel (id,codigo, descripcion,cod_eoi) VALUES(?,?,?,?)");
            stmt.setInt(1, n.getId());
            stmt.setString(2, Character.toString(n.getCodigo()));
            stmt.setString(3, n.getDescripcion());
            stmt.setInt(4, eoi.getCodigo());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        for (Curso c : n.getCursos()) {
            this.cursoDAO.insertaCursos(c, con, n);
        }

    }

    public int elimina(Connection con, Nivel n) throws SQLException {
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement("DELETE  FROM nivel WHERE id=?");
            stmt.setInt(1, n.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR, ESTÁS INTENTANDO ELIMINAR UN OBJETO QUE CONTIENE OBJETOS DENTRO, POR FAVOR, ELIMINA PRIMERO LOS OBJETOS DEL INTERIOR");
            return 0;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return 1;
    }

    public void actualiza(Connection con, Nivel n) throws SQLException {
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement("UPDATE nivel SET codigo=?, descripcion=? WHERE id=?");

            stmt.setString(1, Character.toString(n.getCodigo()));
            stmt.setString(2, n.getDescripcion());
            stmt.setInt(3, n.getId());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public ArrayList<Nivel> obtenerNiveles(Connection con, int claveAjena) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Nivel n;
        ArrayList<Nivel> listaNiveles = new ArrayList<Nivel>();
        ArrayList<Curso> listaCursos;
        CursoDAO cursoDAO = new CursoDAO();
        int id;
        String descripcion;
        char codigo;
        try {
            stmt = con.prepareStatement("SELECT * FROM nivel WHERE cod_eoi=?");
            stmt.setInt(1, claveAjena);
            rs = stmt.executeQuery();

            while (rs.next()) {
                id = rs.getInt("id");
                codigo = rs.getString("codigo").charAt(0);
                descripcion = rs.getString("descripcion");
                
                n = new Nivel(codigo, descripcion, id);

                listaCursos = cursoDAO.obtenerCursos(con, id);
                n.setCursos(listaCursos);
                listaNiveles.add(n);

            }
        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return listaNiveles;
    }

    public Nivel obtenerNivelPorId(Connection con, Nivel n) throws SQLException {

        PreparedStatement stmt = null;
        ResultSet rs = null;
        NivelDAO nivelDAO = new NivelDAO();
        ArrayList<Nivel> listaNiveles;
        ArrayList<Curso> listaCursos;
        CursoDAO cursoDAO= new CursoDAO();
        try {
            stmt = con.prepareStatement("SELECT * FROM nivel WHERE id=?");
            stmt.setInt(1, n.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
              
                n.setCodigo(rs.getString("codigo").charAt(0));
                n.setDescripcion(rs.getString("descripcion"));
                
                listaCursos=cursoDAO.obtenerCursos(con, n.getId());
                n.setCursos(listaCursos);
            }
            

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return n;
    }
}
