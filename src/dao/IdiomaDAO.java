/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Curso;
import modelo.Idioma;
import modelo.Nivel;

/**
 *
 * @author Sandra
 */
public class IdiomaDAO {

    private int id_idioma;

    public IdiomaDAO(int id_idioma) {
        this.id_idioma = 0;
    }

    public IdiomaDAO() {

    }

    public void insertaIdiomas(Idioma i, Connection con, Curso c) throws SQLException {
        PreparedStatement stmt = null;
        //creamos el id autonumérico, cada vez que insertamos, reenumerará
        this.id_idioma++;

        try {
            stmt = con.prepareStatement("INSERT INTO idioma (id,descripcion, grupos, total_alumnos, id_curso) VALUES(?,?,?,?,?)");
            stmt.setInt(1, this.id_idioma);
            stmt.setString(2, i.getDescripcion());
            stmt.setInt(3, i.getGrupos());
            stmt.setInt(4, i.getTotalAlumnos());
            stmt.setInt(5, c.getId());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

    }

    public int elimina(Connection con, Idioma i) throws SQLException {
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement("DELETE FROM idioma WHERE id=?");
            stmt.setInt(1, i.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR, ESTÁS INTENTANDO ELIMINAR UN OBJETO QUE CONTIENE OBJETOS DENTRO, POR FAVOR, ELIMINA PRIMERO LOS OBJETOS DEL INTERIOR");
            return 0;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return 1;
    }

    public void actualiza(Connection con, Idioma i) throws SQLException {
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement("UPDATE idioma SET descripcion=?, grupos=?, total_alumnos=? WHERE id=?");

            stmt.setString(1, i.getDescripcion());
            stmt.setInt(2, i.getGrupos());
            stmt.setInt(3, i.getTotalAlumnos());
            stmt.setInt(4, i.getId());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public ArrayList<Idioma> obtenerIdiomas(Connection con, int claveAjena) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Idioma i;
        ArrayList<Idioma> listaIdiomas = new ArrayList<Idioma>();
        int id,grupos,totalAlumnos;
        String descripcion;

        try {
            stmt = con.prepareStatement("SELECT * FROM idioma WHERE id_curso=?");

               stmt.setInt(1, claveAjena);
            rs = stmt.executeQuery();

            while (rs.next()) {
                id=rs.getInt("id");
                descripcion=rs.getString("descripcion");
                grupos=rs.getInt("grupos");
                totalAlumnos=rs.getInt("total_alumnos");

                i=new Idioma(descripcion, grupos,totalAlumnos,id);
                
                listaIdiomas.add(i);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return listaIdiomas;
    }
     public Idioma obtenerIdiomaPorId(Connection con, Idioma i) throws SQLException {
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM idioma WHERE id=?");

            stmt.setInt(1, i.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
                
                i.setDescripcion(rs.getString("descripcion"));
                i.setGrupos(rs.getInt("grupos"));
                i.setTotalAlumnos(rs.getInt("total_alumnos"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return i;
    }
}
