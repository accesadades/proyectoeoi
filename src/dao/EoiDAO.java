/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Curso;
import modelo.Eoi;
import modelo.EscuelasOficialesIdiomas;
import modelo.Idioma;
import modelo.Nivel;

/**
 *
 * @author Sandra
 */
public class EoiDAO {

    private NivelDAO nivelDAO;

    public EoiDAO() {
        this.nivelDAO = new NivelDAO();
    }

    public void inserta(Connection con, Eoi eoi) throws SQLException {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO eoi (codigo, cod_provincia, nom_provincia,cod_municipio, nom_municipio) VALUES(?,?,?,?,?)");
            stmt.setInt(1, eoi.getCodigo());
            stmt.setInt(2, eoi.getCodProvincia());
            stmt.setString(3, eoi.getNomProvincia());
            stmt.setInt(4, eoi.getCodMunicipio());
            stmt.setString(5, eoi.getNomMunicipio());

            stmt.executeUpdate();

        } catch (MySQLIntegrityConstraintViolationException exception) {
            System.out.println("ERROR. DATOS YA INSERTADOS.");

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        for (Nivel n : eoi.getNiveles()) {
            this.nivelDAO.insertaNiveles(n, con, eoi);
        }
    }

    public int elimina(Connection con, Eoi eoi) throws SQLException {
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement("DELETE FROM eoi WHERE codigo=?");
            stmt.setInt(1, eoi.getCodigo());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR, ESTÁS INTENTANDO ELIMINAR UN OBJETO QUE CONTIENE OBJETOS DENTRO, POR FAVOR, ELIMINA PRIMERO LOS OBJETOS DEL INTERIOR");
            return 0;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return 1;
    }

    public void actualiza(Connection con, Eoi eoi) throws SQLException {
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement("UPDATE eoi SET cod_provincia=?, nom_provincia=?, cod_municipio=?, nom_municipio=? WHERE codigo=?");

            stmt.setInt(1, eoi.getCodProvincia());
            stmt.setString(2, eoi.getNomProvincia());
            stmt.setInt(3, eoi.getCodMunicipio());
            stmt.setString(4, eoi.getNomMunicipio());
            stmt.setInt(5, eoi.getCodigo());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public EscuelasOficialesIdiomas obtenerEois(Connection con) throws SQLException {
        EscuelasOficialesIdiomas escuelas = new EscuelasOficialesIdiomas();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Eoi eoi;
        String nomMunicipio, nomProvincia;
        int codigo, codProvincia, codMunicipio;
        NivelDAO nivelDAO = new NivelDAO();
        ArrayList<Nivel> listaNiveles= new ArrayList<Nivel>();
        try {

            stmt = con.prepareStatement("SELECT * FROM eoi");
            rs = stmt.executeQuery();

            while (rs.next()) {
                codigo = rs.getInt("codigo");
                codProvincia = rs.getInt("cod_provincia");
                nomProvincia = rs.getString("nom_provincia");
                codMunicipio = rs.getInt("cod_municipio");
                nomMunicipio = rs.getString("nom_municipio");

                eoi = new Eoi(codigo, codProvincia, nomProvincia, codMunicipio, nomMunicipio);
                listaNiveles=nivelDAO.obtenerNiveles(con, codigo);
                eoi.setNiveles(listaNiveles);
                escuelas.add(eoi);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return escuelas;
    }

    public Eoi obtenerEoiPorId(Connection con, Eoi eoi) throws SQLException {

        PreparedStatement stmt = null;
        ResultSet rs = null;
        NivelDAO nivelDAO = new NivelDAO();
        ArrayList<Nivel> listaNiveles;
        try {
            stmt = con.prepareStatement("SELECT * FROM eoi WHERE codigo=?");

            stmt.setInt(1, eoi.getCodigo());
            rs = stmt.executeQuery();

            while (rs.next()) {
               
             
                eoi.setCodProvincia(rs.getInt("cod_provincia"));
                eoi.setNomProvincia(rs.getString("nom_provincia"));
                eoi.setCodMunicipio(rs.getInt("cod_municipio"));
                eoi.setNomMunicipio(rs.getString("nom_municipio"));

                listaNiveles = nivelDAO.obtenerNiveles(con, eoi.getCodigo());
                eoi.setNiveles(listaNiveles);

            }

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return eoi;
    }

    public EscuelasOficialesIdiomas buscarEscuelasPorProvincia(Connection con, String provincia) throws SQLException {
        EscuelasOficialesIdiomas escuelas = new EscuelasOficialesIdiomas();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Eoi eoi = new Eoi();
        ArrayList<Nivel> listaNiveles;
        NivelDAO nivelDAO = new NivelDAO();
        try {
            stmt = con.prepareStatement("SELECT * FROM eoi WHERE nom_provincia=?");

            stmt.setString(1, provincia);
            rs = stmt.executeQuery();

            while (rs.next()) {
                eoi.setCodigo(rs.getInt("codigo"));
                eoi.setCodProvincia(rs.getInt("cod_provincia"));
                eoi.setNomProvincia(rs.getString("nom_provincia"));
                eoi.setCodMunicipio(rs.getInt("cod_municipio"));
                eoi.setNomMunicipio(rs.getString("nom_municipio"));

                listaNiveles = nivelDAO.obtenerNiveles(con, eoi.getCodigo());
                eoi.setNiveles(listaNiveles);
                escuelas.add(eoi);
            }

        } catch (SQLException ex) {
            Logger.getLogger(EoiDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return escuelas;
    }

}
